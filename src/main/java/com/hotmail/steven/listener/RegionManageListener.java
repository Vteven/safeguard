package com.hotmail.steven.listener;

import java.util.HashMap;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.hotmail.steven.SafeGuard;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class RegionManageListener implements Listener {

	private SafeGuard plugin;
	private HashMap<UUID, Long> cooldowns;
	
	public RegionManageListener(SafeGuard plugin)
	{
		this.plugin = plugin;
		cooldowns = new HashMap<UUID, Long>();
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent evt) {
		
		Player p = evt.getPlayer();
		
		if(evt.getAction() == Action.LEFT_CLICK_BLOCK || evt.getAction() == Action.RIGHT_CLICK_BLOCK && canInteract(p, 20)) {
			
			Block b = evt.getClickedBlock();
			ApplicableRegionSet regions = WGBukkit.getRegionManager(evt.getPlayer().getWorld()).getApplicableRegions(b.getLocation());
			Location bLoc = b.getLocation();
			if(regions.size() > 0) {
				
				String loc = "sg_".concat(Integer.toString(bLoc.getBlockX())).concat(Integer.toString(bLoc.getBlockY())).concat(Integer.toString(bLoc.getBlockZ()));
				
				for(ProtectedRegion region : regions.getRegions()) {
					
					if(region.getId().equals(loc)) {
						
						if(region.isOwner(WGBukkit.getPlugin().wrapPlayer(evt.getPlayer()))) {
							
							
							
							break;
							
						}
						
					}
				}
			}
			
			cooldowns.put(p.getUniqueId(), System.currentTimeMillis());
		}
		
	}
	
	/**
	 * Check whether a player can interact after a specific cooldown
	 * @param p
	 * @return
	 */
	private boolean canInteract(Player p, long cooldown) {
		
		if(cooldowns.containsKey(p.getUniqueId())) {
			
			long lastInteract = cooldowns.get(p.getUniqueId());
			// Convert to MS
			lastInteract = lastInteract / 1000;
			long currentTime = System.currentTimeMillis() / 1000;
			// Check if its been long enough
			if(currentTime - lastInteract > cooldown) {
				
				return true;
				
			} else {
				
				return false;
				
			}
			
		}
			
		return true;
		
	}
	
}
