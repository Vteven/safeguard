package com.hotmail.steven.listener;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

import com.hotmail.steven.SafeGuard;
import com.hotmail.steven.SafeGuardBlock;
import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.bukkit.RegionContainer;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.md_5.bungee.api.ChatColor;

public class RegionCreateListener implements Listener {
	
	private SafeGuard plugin;
	
	public RegionCreateListener(SafeGuard plugin) {
		
		this.plugin = plugin;
		
	}

	@EventHandler
	public void onBlockPlace(BlockPlaceEvent evt) 
	{
		Block b = evt.getBlock();
		Location loc = b.getLocation();
		ItemStack item = evt.getItemInHand();
		SafeGuardBlock safeGuardBlock = SafeGuard.getSafeGuardBlock(item);
		
		if(safeGuardBlock != null) {
			
			if(WGBukkit.getPlugin().canBuild(evt.getPlayer(), b)) {
				Player p = evt.getPlayer();
				// Radius
				int r = safeGuardBlock.getRadius();
				// Get worldguard player
				LocalPlayer localPlayer = WGBukkit.getPlugin().wrapPlayer(p);
				//ApplicableRegionSet set = WGBukkit.getRegionManager(w).getApplicableRegions(b.getLocation());
				
				Location bLoc = b.getLocation();
				BlockVector min = new BlockVector(loc.getBlockX() - r, loc.getBlockY() - r, loc.getBlockZ() - r);
				BlockVector max = new BlockVector(loc.getBlockX() + r, loc.getBlockY() + r, loc.getBlockZ() + r);
				// Create the new region
				ProtectedRegion region = new ProtectedCuboidRegion("sg_" + bLoc.getBlockX() + "_" + bLoc.getBlockY() + "_" + bLoc.getBlockZ(), min, max);
				// Get the overlapping regions for the new region
				ApplicableRegionSet overlapping = WGBukkit.getPlugin().getRegionContainer().get(bLoc.getWorld()).getApplicableRegions(region);
			
				if(overlapping.isOwnerOfAll(localPlayer)) {
			
					int priority = 0;
					// Find the highest priority region that this region overlaps
					for(ProtectedRegion compareRegion : overlapping.getRegions()) {
						
						if(compareRegion.getPriority() > priority) {
							
							priority = compareRegion.getPriority();
							
						}
						
					}
					
					priority++;
					region.setPriority(priority);
					
					// Set default flags
					for(String[] args : safeGuardBlock.getDefaultFlags()) {
						
						plugin.setFlag(args, region, p);
						
					}
					
					RegionContainer container = WGBukkit.getPlugin().getRegionContainer();
					RegionManager regions = container.get(bLoc.getWorld());
					regions.addRegion(region);
					
					p.sendMessage(ChatColor.GREEN + "Your land is protected, right click the block to manage it");
				
				} else {
					
					p.sendMessage(ChatColor.RED + "Ops, looks like someone else owns this land");
					
				}
			}
		}
	}
	
}
