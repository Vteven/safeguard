package com.hotmail.steven.listener;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

import com.sk89q.worldguard.bukkit.RegionContainer;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.md_5.bungee.api.ChatColor;

public class RegionRemoveListener implements Listener {

	@EventHandler
	public void onBlockBreak(BlockBreakEvent evt) {
		
		ApplicableRegionSet regions = WGBukkit.getRegionManager(evt.getPlayer().getWorld()).getApplicableRegions(evt.getBlock().getLocation());
		Location bLoc = evt.getBlock().getLocation();
		if(regions.size() > 0) {
			
			String loc = "sg_".concat(Integer.toString(bLoc.getBlockX())).concat(Integer.toString(bLoc.getBlockY())).concat(Integer.toString(bLoc.getBlockZ()));
			
			for(ProtectedRegion region : regions.getRegions()) {
				
				if(region.getId().equals(loc)) {
					
					if(region.isOwner(WGBukkit.getPlugin().wrapPlayer(evt.getPlayer()))) {
						
						// Remove the region
						RegionContainer container = WGBukkit.getPlugin().getRegionContainer();
						RegionManager regionContainer = container.get(bLoc.getWorld());
						regionContainer.removeRegion(loc);
						
						evt.getPlayer().sendMessage(ChatColor.GREEN + "This land is no longer protected");
						
						break;
						
					} else {
						
						evt.setCancelled(true);
						evt.getPlayer().sendMessage(ChatColor.RED + "You do not own this area!");
						
					}

				}
				
			}
		
		}
		
	}
	
}
