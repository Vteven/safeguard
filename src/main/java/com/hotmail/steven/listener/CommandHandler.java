package com.hotmail.steven.listener;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.hotmail.steven.SafeGuard;

public class CommandHandler implements CommandExecutor {

	private SafeGuard plugin;
	
	public CommandHandler(SafeGuard plugin)
	{
		this.plugin = plugin;
	}
	
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		
		if(args.length > 0 && args[0].equalsIgnoreCase("reload")) {
			
			plugin.reload();
			sender.sendMessage("SafeGuard was reloaded successfully");
			
			return true;
			
		}
		
		return false;
	}

}
