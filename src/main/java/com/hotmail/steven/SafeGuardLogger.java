package com.hotmail.steven;

import java.util.logging.Level;

import org.bukkit.Bukkit;

public class SafeGuardLogger {

	public static void log(Level level, String message) {
		
		Bukkit.getLogger().log(level, "[SafeGuard] " + message);
		
	}
	
}
