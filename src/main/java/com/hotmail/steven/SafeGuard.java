package com.hotmail.steven;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.hotmail.steven.listener.CommandHandler;
import com.hotmail.steven.listener.RegionCreateListener;
import com.hotmail.steven.listener.RegionManageListener;
import com.hotmail.steven.listener.RegionRemoveListener;
import com.hotmail.steven.menu.Button;
import com.hotmail.steven.menu.ButtonListener;
import com.hotmail.steven.menu.InputListener;
import com.hotmail.steven.menu.MenuBuilder;
import com.hotmail.steven.menu.MenuBuilderListener;
import com.hotmail.steven.util.ItemUtil;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.flags.BooleanFlag;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.DoubleFlag;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.IntegerFlag;
import com.sk89q.worldguard.protection.flags.RegionGroup;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.flags.StateFlag.State;
import com.sk89q.worldguard.protection.flags.StringFlag;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class SafeGuard extends JavaPlugin
{
	
	private static List<SafeGuardBlock> blocks;
	private static FileConfiguration cfg;
	
    public void onEnable() 
    {
    	WorldGuardPlugin plugin = WGBukkit.getPlugin();
    	
    	if(plugin == null) {
    		SafeGuardLogger.log(Level.INFO, "SafeGuard shutting down due to the WorldGuard dependency not loading correctly.");
    	} else {
    		
    		this.getCommand("sg").setExecutor(new CommandHandler(this));
    		this.saveDefaultConfig();
    		SafeGuard.cfg = this.getConfig();
    		blocks = new ArrayList<SafeGuardBlock>();
    		this.loadSafeGuardBlocks();
    		Bukkit.getPluginManager().registerEvents(new MenuBuilderListener(this), plugin);
    		Bukkit.getPluginManager().registerEvents(new RegionCreateListener(this), plugin);
    		Bukkit.getPluginManager().registerEvents(new RegionManageListener(this), plugin);
    		Bukkit.getPluginManager().registerEvents(new RegionRemoveListener(), plugin);
    	}
    }
    
    /**
     * Load all of the safe guard types from config
     */
    private void loadSafeGuardBlocks() 
    {
    	for(String key : SafeGuard.cfg.getConfigurationSection("protection-stones").getKeys(false)) {
    		ConfigurationSection section = getConfig().getConfigurationSection("protection-stones." + key);
    		
    		SafeGuardLogger.log(Level.INFO, "(" + key + ") loading");
    		
    		Material mat = Material.AIR;
    		int radius = -1;
    		
    		if(section.contains("block")) {
    			mat = Material.valueOf(section.getString("block").toUpperCase());
    		}
    		
    		if(section.contains("radius")) {
    			radius = section.getInt("radius");
    		}
    		
    		if(mat != Material.AIR && radius != -1) {
    			
    			SafeGuardBlock blockType = new SafeGuardBlock(mat, radius);
    			
        		if(section.contains("meta.title")) {
        			
        			SafeGuardLogger.log(Level.INFO, "(" + key + ") title found");
        			
        			blockType.setTitle(section.getString("meta.title"));
        			
        		}
        		
        		if(section.contains("meta.lore")) {
        			
        			SafeGuardLogger.log(Level.INFO, "(" + key + ") lore found");
        			
        			blockType.setLore(section.getStringList("meta.lore"));
        			
        		}
        		
        		if(section.isList("flags")) {
        			
        			int loaded = 0;
        			for(String strFlag : section.getStringList("flags")) {
        				
        				String[] parts = strFlag.split("\\|");
        				Flag<?> flag = DefaultFlag.fuzzyMatchFlag(WGBukkit.getPlugin().getFlagRegistry(), parts[0]);
        				if(flag == null) {
        					
        					SafeGuardLogger.log(Level.INFO, "(" + key + ") failed to load flag " + parts[0]);
        					
        				} else {
        					
	        				blockType.addDefaultFlag(parts);
	        				loaded++;
        				
        				}
        				
        			}
        			
        			SafeGuardLogger.log(Level.INFO, "(" + key + ") " + loaded + " flags loaded");
        			
        		}
        		
    		} else {
    			SafeGuardLogger.log(Level.INFO, "(" + key + ") failed to load block");
    		}
    	}
    }
    
    /**
     * Get the SafeGuardBlock for an ItemStack.
     * Matches the material and if existing the title and lore
     * @param item
     * @return
     */
    public static SafeGuardBlock getSafeGuardBlock(ItemStack item) 
    {
    	for(SafeGuardBlock blockType : blocks) {
    		
    		if(blockType.getMaterial() == item.getType()) {
    			
    			if(item.isSimilar(blockType.getItemStack())) {
    				
    				return blockType;
    				
    			}
    			
    		}
    		
    	}
    	
    	return null;
    }
    
    /**
     * Creates a WorldGuard protected region from a SafeGuardBlock, Location
     * and Player
     * @param block
     * @param loc
     * @param p
     */
    public void createFromBlock(SafeGuardBlock block, Location loc, Player p) 
    {
    	
    }
   
    public void setFlag(String[] args, ProtectedRegion region, Player p) {
        Flag<?> rawFlag = DefaultFlag.fuzzyMatchFlag(WGBukkit.getPlugin().getFlagRegistry(), args[1]);
        if ((rawFlag instanceof StateFlag)) {
          StateFlag flag = (StateFlag)rawFlag;
          if (args[2].equalsIgnoreCase("default")) {
            region.setFlag(flag, flag.getDefault());
            region.setFlag(flag.getRegionGroupFlag(), null);
            p.sendMessage(ChatColor.YELLOW + args[1] + " flag has been set.");
          } else {
            RegionGroup group = null;
            if (java.util.Arrays.toString(args).contains("-g")) {
              int i = 0;
              for (String s : args) {
                i++;
                if (s.equalsIgnoreCase("-g")) {
                  group = getRegionGroup(args[i]);
                }
              }
            }
            if (java.util.Arrays.toString(args).contains("allow")) {
              region.setFlag(flag, StateFlag.State.ALLOW);
              if (group != null) {
                region.setFlag(flag.getRegionGroupFlag(), group);
              }
              p.sendMessage(ChatColor.YELLOW + args[1] + " flag has been set.");
            } else if (java.util.Arrays.toString(args).contains("deny")) {
              region.setFlag(flag, StateFlag.State.DENY);
              if (group != null) {
                region.setFlag(flag.getRegionGroupFlag(), group);
              }
              p.sendMessage(ChatColor.YELLOW + args[1] + " flag has been set.");
            }
            else if (group != null) {
              region.setFlag(flag.getRegionGroupFlag(), group);
              p.sendMessage(ChatColor.YELLOW + args[1] + " flag has been set.");
            } else {
              p.sendMessage(ChatColor.YELLOW + args[1] + " flag has " + ChatColor.RED + "not" + ChatColor.YELLOW + " been set.");
            }
          }
        }
        else if ((rawFlag instanceof DoubleFlag)) {
          DoubleFlag flag = (DoubleFlag)rawFlag;
          if (args[2].equalsIgnoreCase("default")) {
            region.setFlag(flag, (Double)flag.getDefault());
            region.setFlag(flag.getRegionGroupFlag(), null);
          } else {
            region.setFlag(flag, Double.valueOf(Double.parseDouble(args[1])));
          }
          p.sendMessage(ChatColor.YELLOW + args[1] + " flag has been set.");
        } else if ((rawFlag instanceof IntegerFlag)) {
          IntegerFlag flag = (IntegerFlag)rawFlag;
          if (args[2].equalsIgnoreCase("default")) {
            region.setFlag(flag, (Integer)flag.getDefault());
            region.setFlag(flag.getRegionGroupFlag(), null);
          } else {
            region.setFlag(flag, Integer.valueOf(Integer.parseInt(args[1])));
          }
          p.sendMessage(ChatColor.YELLOW + args[1] + " flag has been set.");
        } else if ((rawFlag instanceof StringFlag)) {
          StringFlag flag = (StringFlag)rawFlag;
          if (args[2].equalsIgnoreCase("default")) {
            region.setFlag(flag, flag.getDefault());
            region.setFlag(flag.getRegionGroupFlag(), null);
          } else {
            String flagValue = com.google.common.base.Joiner.on(" ").join(args).substring(args[0].length() + args[1].length() + 2);
            String msg = flagValue.replaceAll("%player%", p.getName());
            region.setFlag(flag, msg);
          }
          p.sendMessage(ChatColor.YELLOW + args[1] + " flag has been set.");
        } else if ((rawFlag instanceof BooleanFlag)) {
          BooleanFlag flag = (BooleanFlag)rawFlag;
          if (args[2].equalsIgnoreCase("default")) {
            region.setFlag(flag, (Boolean)flag.getDefault());
            region.setFlag(flag.getRegionGroupFlag(), null);
            p.sendMessage(ChatColor.YELLOW + args[1] + " flag has been set.");
          }
          else if (args[2].equalsIgnoreCase("true")) {
            region.setFlag(flag, Boolean.valueOf(true));
            p.sendMessage(ChatColor.YELLOW + args[1] + " flag has been set.");
          } else if (args[2].equalsIgnoreCase("false")) {
            region.setFlag(flag, Boolean.valueOf(false));
            p.sendMessage(ChatColor.YELLOW + args[1] + " flag has been set.");
          }
        }
    }
      
    private RegionGroup getRegionGroup(String arg)
    {
    	return RegionGroup.valueOf(arg.toUpperCase());
    }
    
    /**
     * Reloads the config and load everything back into memory
     */
    public void reload() {
    	
    	this.reloadConfig();
    	blocks.clear();
    	loadSafeGuardBlocks();
    	
    }
    
    /**
     * Show a menu for the specified player. Uses ProtectedRegion
     * to initialize the button icons etc
     * @param p
     * @param region
     */
    public void showMenu(Player p, final ProtectedRegion region)
    {
		final MenuBuilder whitelistMenu = new MenuBuilder();
		whitelistMenu.size(45).title("&c&lManage whitelist");
		whitelistMenu.button(44, new Button(44, ItemUtil.item(Material.PAPER, 1, "&aNext page")), new ButtonListener()
		{
			@Override
			public void onClick(Button button, Player player)
			{
				
			}
		});
		
		whitelistMenu.button(36, new Button(36, ItemUtil.item(Material.PAPER, 1, "&aPrevious page")), new ButtonListener()
		{
			@Override
			public void onClick(Button button, Player player)
			{
				
			}
		});
		
		whitelistMenu.button(40, new Button(40, ItemUtil.item(Material.PAPER, 1, "&aWhitelist player", "&eClick to add a player|&eto the whitelist")), new InputListener()
		{
			@Override
			public void onEnable(Button button, Player player)
			{
				// Get the whitelist iterator
				List<UUID> members = new ArrayList<UUID>(region.getMembers().getUniqueIds());
				// Update the menu, removing any old skulls
				whitelistMenu.update();
				for(int i=0;i<members.size();i++)
				{
					// Get our current player
					OfflinePlayer next = Bukkit.getOfflinePlayer(members.get(i));
					// Create a button for the player so they can click and remove them from the whitelist
					whitelistMenu.button(i, new Button(i, ItemUtil.item(Material.APPLE, 1, "&a" + next.getName(), "&eClick to remove")), new ButtonListener(next.getUniqueId())
					{
						@Override
						public void onClick(Button button, Player player)
						{
							region.getMembers().removePlayer((UUID)getData()[0]);
							whitelistMenu.removeButton(button.getPosition());
						}
					});
					i++;
				}
				whitelistMenu.update();
			}
			
			@Override
			public void onClick(Button button, Player player)
			{
				player.sendMessage("Enter a player to whitelist:");
			}
			
			@Override
			public void onInput(Player player, String message)
			{
				OfflinePlayer whitelist = Bukkit.getPlayer(message);
				if(whitelist != null)
				{
					region.getMembers().addPlayer(whitelist.getUniqueId());
					player.sendMessage("Player whitelisted successfully");
				} else
				{
					player.sendMessage("You have entered an unknown player");
				}
			}
		});
    	
    	// Flags menu
		final MenuBuilder flagsMenu = new MenuBuilder();
		flagsMenu.size(9).title("&c&lManage flags");
		flagsMenu.button(0, new Button(0, ItemUtil.item(Material.PAPER, 1, "&7Entry message", "&3none")), new InputListener()
		{
			@Override
			public void onEnable(Button button, Player player)
			{
				String welcomeMessage = region.getFlag(DefaultFlag.GREET_MESSAGE);
				button.lore(welcomeMessage);
				flagsMenu.update();
			}
			
			@Override
			public void onInput(Player player, String message)
			{
				// Create our welcome flag and set the message value
				region.setFlag(DefaultFlag.GREET_MESSAGE, message);
				player.sendMessage("Welcome message updated");
			}
			
			@Override
			public void onClick(Button button, Player player)
			{
				player.sendMessage("Enter a new welcome message:");
			}
		});
    	
    	// Main menu
    	MenuBuilder mainMenu = new MenuBuilder();
		mainMenu = new MenuBuilder();
		mainMenu.size(9).title("Manage protection");
		mainMenu.button(0, new Button(0, ItemUtil.item(Material.GLASS, 1, "&aShow area")), new ButtonListener()
		{
			@Override
			public void onClick(Button button, Player player)
			{
				
			}
		});
		
		mainMenu.button(1, new Button(1, ItemUtil.item(Material.PAPER, 1, "&aEdit flags")), new ButtonListener()
		{
			@Override
			public void onClick(Button button, Player player)
			{
				flagsMenu.show(player);
			}
		});
		
		mainMenu.button(2, new Button(2, ItemUtil.item(Material.PAPER, 1, "&aManage whitelist")), new ButtonListener()
		{
			@Override
			public void onClick(Button button, Player player)
			{
				whitelistMenu.show(player);
			}
		});
    }

}
