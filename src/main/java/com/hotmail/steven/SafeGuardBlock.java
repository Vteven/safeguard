package com.hotmail.steven;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.sk89q.worldedit.blocks.BlockType;
import com.sk89q.worldguard.protection.flags.Flag;
import com.sk89q.worldguard.protection.flags.StateFlag;

public class SafeGuardBlock {
	
	private Material mat = Material.AIR;
	private int radius = -1;
	private String title;
	private List<String> lore;
	private List<String[]> defaultFlags;

	public SafeGuardBlock(Material mat, int radius)
	{
		this.mat = mat;
		this.radius = radius;
		defaultFlags = new ArrayList<String[]>();
	}
	
	public void setTitle(String title)
	{
		this.title = title;
	}
	
	public void setLore(List<String> lore) 
	{
		this.lore = lore;
	}
	
	public Material getMaterial() 
	{
		return mat;
	}
	
	public int getRadius() 
	{
		return radius;
	}
	
	public boolean hasTitle()
	{
		return !title.equals("");
	}
	
	public String getTitle() 
	{
		return title;
	}
	
	public boolean hasLore()
	{
		return lore.size() > 0;
	}
	
	public List<String> getLore() 
	{
		return lore;
	}
	
	public List<String[]> getDefaultFlags() 
	{
		return defaultFlags;
	}
	
	public void addDefaultFlag(String[] strParts) 
	{
		defaultFlags.add(strParts);
	}
	
	public ItemStack getItemStack() 
	{
		ItemStack item = new ItemStack(mat);
		ItemMeta im = item.getItemMeta();
		if(hasTitle()) {
			im.setDisplayName(ChatMessages.color(getTitle()));
		}
		if(hasLore()) {
			im.setLore(this.lore);
		}
		item.setItemMeta(im);
		return item;
	}
}
